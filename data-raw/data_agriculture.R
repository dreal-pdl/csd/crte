## code to prepare `data_agriculture` dataset goes here
rm(list=ls())
library(purrr)
library(tidyr)
library(dplyr)
library(COGiter)
library(tricky)
library(lubridate)
library(stringr)

# une fonction sale pour passer aux communes 2021, en PdL seulement une fusion de communes dans le 53
com_2021 <- function(dataset = data_cogifiee) {
  data_com <- filter(dataset, TypeZone == "Communes") %>%
    select(-Zone, -TypeZone) %>%
    passer_au_cog_a_jour(code_commune = CodeZone, garder_info_supra = F) %>%
    right_join(COGiter::liste_zone %>% select(Zone, CodeZone, TypeZone), .,
               by = c("CodeZone" = "DEPCOM"))

  filter(dataset, TypeZone != "Communes") %>%
    bind_rows(data_com, .)
}


load('inst/extdata/cogifiee_chargement_agri.RData')
data_agriculture_rga <- data_cogifiee %>%
  com_2021() %>%
  filtrer_cog(reg = '52', garder_supra = '>') %>%
  filter(variable %in% c('exploitation','superficie_agricole_utilisee_siege')) %>%
  pivot_wider(names_from = "variable",
              values_from = "valeur")

# load('inst/extdata/indicateur_agri_cogiter.RData')

load('inst/extdata/indicateur_agriculture_bio.RData')
data_agriculture_bio <- indicateur_agriculture_bio %>%
  filtrer_cog(reg = '52',garder_supra = '>') %>%
  filter(variable %in% c('surface_totale_engagee_bio','nb_exploitation_engagee_bio')) %>%
  pivot_wider(names_from = "variable",
              values_from = "valeur") %>%
  select(-Zone) %>%
  left_join(COGiter::liste_zone %>%
              select(Zone,CodeZone,TypeZone)
  )

list<-ls()[stringr::str_detect(ls(),"data_agriculture")]

data_agriculture <- map(list, ~ get(.x) %>%
                             mutate(date=year(date))) %>%
  reduce(full_join) %>%
  bind_rows(filter(., CodeZone == "85113") %>%
              mutate(TypeZone = factor("Epci", levels = levels(.$TypeZone)),
                     CodeZone = factor("85113ZZZZ", levels = c(levels(.$CodeZone), "85113ZZZZ"))), .
            )

usethis::use_data(data_agriculture, overwrite = TRUE)
