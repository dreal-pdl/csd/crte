## code to prepare `data_mobilite` dataset goes here
rm(list=ls())
library(purrr)
library(tidyr)
library(dplyr)
library(COGiter)
library(tricky)
library(lubridate)

# une fonction sale pour passer aux communes 2021, en PdL seulement une fusion de communes dans le 53
com_2021 <- function(dataset = data_cogifiee) {
  data_com <- filter(dataset, TypeZone == "Communes") %>%
    select(-Zone, -TypeZone) %>%
    passer_au_cog_a_jour(code_commune = CodeZone, garder_info_supra = F) %>%
    right_join(COGiter::liste_zone %>% select(Zone, CodeZone, TypeZone), .,
               by = c("CodeZone" = "DEPCOM"))

  filter(dataset, TypeZone != "Communes") %>%
    bind_rows(data_com, .)
}

load('inst/extdata/indicateur_rsvero.RData')
data_mobilite_parc_auto <- indicateur_rsvero %>%
  com_2021() %>%
  filter(variable %in% c('part_vehicule_electrique_hydrogene_hybride_rechargeable_pourcent')
         ) %>%
  filtrer_cog(reg = '52',garder_supra = '>') %>%
  pivot_wider(names_from = "variable", values_from = "valeur")

load('inst/extdata/indicateur_bimotor_menages.RData')
data_mobilite_bimotor_menages <- indicateur_bimotor_menages %>%
  com_2021() %>%
  filtrer_cog(reg = '52',garder_supra = '>') %>%
  pivot_wider(names_from = "variable", values_from = "valeur")


load('inst/extdata/cogifiee_chargement_mode_transport_dom_travail.RData')
data_mobilite_mode_doux <- data_cogifiee %>%
  com_2021() %>%
  filtrer_cog(reg = '52',garder_supra = '>') %>%
  filter(variable != 'mode_total') %>%
  group_by(TypeZone,Zone,CodeZone,date) %>%
  mutate(valeur = valeur*100/sum(valeur)) %>%
  pivot_wider(names_from = "variable", values_from = "valeur")


load('inst/extdata/cogifiee_chargement_osm_parkings_velo.RData')
data_mobilite_capacite_parking_velo <- data_cogifiee %>%
  com_2021() %>%
  filtrer_cog(reg = '52',garder_supra = '>') %>%
  pivot_wider(names_from = "variable", values_from = "valeur")

load('inst/extdata/indicateur_pist_cycl_pop_legale.RData')
data_mobilite_piste_cyclable_par_hab <- indic_lg_cycl_par_hab %>%
  filtrer_cog(reg = '52', garder_supra = '>') %>%
  #com2021 : il faudrait revenir aux données sources pour pouvoir faire une moyenne pondéré pop pour la commune fusionnée de Vimartin sur Orthe
  mutate(valeur = if_else(CodeZone == "53249", NA_real_, valeur)) %>%
  pivot_wider(names_from = "variable", values_from = "valeur")

load('inst/extdata/cogifiee_chargement_bornes_recharges_vehicules_elec.RData')
data_mobilite_bornes_recharges_vehicules_elec <- data_cogifiee %>%
  com_2021() %>%
  filtrer_cog(reg = '52', garder_supra = '>') %>%
  pivot_wider(names_from = "variable", values_from = "valeur")

load('inst/extdata/ref_aom_epci.RData')
data_mobilite_aom <- aom %>%
  mutate(TypeZone = 'Epci',CodeZone = epci, date = ymd('20210101'),
         aom = ifelse(AOM,'oui','non')) %>%
  select(-epci,-AOM) %>%
  left_join(epci %>% select(CodeZone = EPCI, Zone=NOM_EPCI))

list <- ls()[stringr::str_detect(ls(),"data_mobilite")]

data_mobilite <- map(list, ~ get(.x) %>%
            mutate(date=year(date))) %>%
  reduce(full_join) %>%
  bind_rows(
    filter(., CodeZone == "85113") %>%
      mutate(TypeZone = "Epci",
             CodeZone = factor("85113ZZZZ", levels = c(levels(.$CodeZone), "85113ZZZZ"))), .
  ) %>%
  mutate(TypeZone = factor(TypeZone, levels = levels(liste_zone$TypeZone)))

usethis::use_data(data_mobilite, overwrite = TRUE)



